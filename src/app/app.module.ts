import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatGridListModule,
  MatSelectModule,
  MatIconModule,
  MatButtonModule
} from '@angular/material';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProductSingleComponent } from './components/product-single/product-single.component';
import { TopHeaderComponent } from './components/header/top-header/top-header.component';
import { MainHeaderComponent } from './components/header/main-header/main-header.component';
import { MainMenuComponent } from './components/header/main-menu/main-menu.component';
import { CurrencyComponent } from './components/header/main-header/currency/currency.component';
import { CartComponent } from './components/header/main-header/cart/cart.component';
import { GalleryComponent } from './components/product-single/gallery/gallery.component';
import { ProductDetailsComponent } from './components/product-single/product-details/product-details.component';
import { AddToCartComponent } from './components/product-single/product-details/add-to-cart/add-to-cart.component';
import { CartService } from './services/cart.service';
import { GalleryService } from './services/gallery.service';
import { NgxGalleryModule } from 'ngx-gallery';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProductSingleComponent,
    TopHeaderComponent,
    MainHeaderComponent,
    MainMenuComponent,
    CurrencyComponent,
    CartComponent,
    GalleryComponent,
    ProductDetailsComponent,
    AddToCartComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatGridListModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxGalleryModule
  ],
  providers: [CartService, GalleryService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartItems;
  constructor() { }

  ngOnInit() {
    this.getCartItems();
  }

  getCartItems() {
    if (localStorage.getItem('cartObj') != null) {
      this.cartItems = JSON.parse(localStorage.getItem('cartObj')).length;
    }
  }

}

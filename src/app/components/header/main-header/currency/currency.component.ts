import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit {
  selected;
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'USD',
      sanitizer.bypassSecurityTrustResourceUrl('assets/usd.svg'));
    iconRegistry.addSvgIcon(
      'EUR',
      sanitizer.bypassSecurityTrustResourceUrl('assets/euro.svg'));
    iconRegistry.addSvgIcon(
      'AED',
      sanitizer.bypassSecurityTrustResourceUrl('assets/aed.svg'));
  }

  ngOnInit() {
    this.selected = 'USD';
  }

}

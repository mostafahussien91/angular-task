import { Component, OnInit } from '@angular/core';
import { GalleryService } from '../../../services/gallery.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  gallery;
  largeImgs;
  thumbnails;
  firstImg;

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  constructor(private galleryServ: GalleryService) { }

  ngOnInit() {
    this.getImages();
    this.galleryOptions = [
      {
        width: 'auto',
        height: '600px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        // preview: false
      }
    ];
  }

  getImages() {
    this.galleryServ.getGallery().subscribe(data => {
      this.gallery = data.gallery.images;
      this.galleryImages = this.gallery.map(res => {
        return { small: res.thumbnail, medium: res.large, big: res.large };
      });
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CartService } from '../../../../services/cart.service';

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.scss']
})
export class AddToCartComponent implements OnInit {

  addToCart: FormGroup;
  colors = ['Red', 'Green', 'Blue'];
  sizes = ['S', 'M', 'L', 'XL'];
  swatches;
  prodQty = 0;
  qtyList = [];
  outOfStockList;
  disabledSize;
  cartObj = [];

  wishlist = false;

  constructor(private formBuilder: FormBuilder, private cartService: CartService) {
  }

  ngOnInit() {
    this.createForm();
    this.getSwatches();
    this.onChanges();
    this.checkWishlist();
  }


  createForm() {
    this.addToCart = this.formBuilder.group({
      color: ['', Validators.required],
      size: ['', Validators.required],
      qty: ['', Validators.required],
    });
  }

  onSubmit() {

    if (localStorage.getItem('cartObj') === null) {
      localStorage.setItem('cartObj', JSON.stringify([this.addToCart.value]));
    } else {
      let exisiting = JSON.parse(localStorage.getItem('cartObj'));
      exisiting.push(this.addToCart.value);
      localStorage.setItem('cartObj', JSON.stringify(exisiting));
    }
    this.addToCart.reset();
  }


  onChanges(): void {
    this.addToCart.valueChanges.subscribe(
      result => {
        if (this.addToCart.get('color').valid) {
          this.disabledSize = this.getOutOfStock(this.addToCart.get('color').value);
        }
        if (this.addToCart.get('color').valid && this.addToCart.get('size').valid) {
          this.getQunatity();
        }
      }
    );
  }

  getSwatches() {
    this.cartService.getSwatches().subscribe(data => {
      this.swatches = data;
    });
  }

  getQunatity() {
    let color = this.addToCart.get('color').value;
    let size = this.addToCart.get('size').value;
    this.prodQty = this.swatches.filter(res => res.color === color && res.size === size);
    this.qtyList = Array.from({ length: this.prodQty[0].qty }, (_, i) => i + 1);
  }

  getOutOfStock(color) {
    this.outOfStockList = this.swatches.filter(res => res.color === color && res.qty === 0);
    return this.outOfStockList[0].size;
  }


  addToWishlist() {
    if (localStorage.getItem('wishlist') != null) {
      localStorage.removeItem('wishlist');
      this.wishlist = false;
    } else {
      localStorage.setItem('wishlist', '1');
      this.wishlist = true;
    }
  }

  checkWishlist() {
    if (localStorage.getItem('wishlist') != null) {
      this.wishlist = true;
    } else {
      this.wishlist = false;
    }
  }
}
